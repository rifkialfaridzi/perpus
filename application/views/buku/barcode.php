<?php if (!defined('BASEPATH')) exit('No direct script acess allowed'); ?>
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-md-2 col-md-offset-5">
                <div class="box box-primary">
                    <div class="box-header with-border">
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- <?php
                        // var_dump($buku);
                        $generator = new Picqer\Barcode\BarcodeGeneratorPNG();
                        // echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($buku->buku_id, $generator::TYPE_CODE_128)) . '">';
                        ?> -->
                        <div style="width: 70%;display: block;margin-left: auto;margin-right: auto;">
                            <h1 style="text-align: center;">Cetak Barcode Buku : <?php echo $buku->title; ?> </h1>

                            <table style="width: 100%;">
                                <tr style="border: 1px dashed black;margin:1em;padding:1em;">
                                    <td style="border: 1px dashed black; padding:2em 1em 2em 1em;margin:1em; text-align:center">
                                        <h3><?php echo $buku->title; ?></h3>
                                        <?php echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($buku->buku_id, $generator::TYPE_CODE_128)) . '">'; ?>
                                        <p style="margin: 0;"><?php echo $buku->buku_id; ?></p>
                                        <p style="margin-bottom: 0;"><b>ISBN</b></p>
                                        <p style="margin: 0;"><?php echo $buku->isbn; ?></p>
                                        <p style="margin-bottom: 0;"><b>Penerbit</b></p>
                                        <p style="margin: 0;"><?php echo $buku->penerbit; ?></p>
                                        <p style="margin-bottom: 0;"><b>Tahun Buku</b></p>
                                        <p style="margin: 0;"><?php echo $buku->thn_buku; ?></p>
                                    </td>
                                    <td style="border: 1px dashed black; padding:2em 1em 2em 1em;margin:1em; text-align:center">
                                        <h3><?php echo $buku->title; ?></h3>
                                        <?php echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($buku->buku_id, $generator::TYPE_CODE_128)) . '">'; ?>
                                        <p style="margin: 0;"><?php echo $buku->buku_id; ?></p>
                                        <p style="margin-bottom: 0;"><b>ISBN</b></p>
                                        <p style="margin: 0;"><?php echo $buku->isbn; ?></p>
                                        <p style="margin-bottom: 0;"><b>Penerbit</b></p>
                                        <p style="margin: 0;"><?php echo $buku->penerbit; ?></p>
                                        <p style="margin-bottom: 0;"><b>Tahun Buku</b></p>
                                        <p style="margin: 0;"><?php echo $buku->thn_buku; ?></p>
                                    </td>
                                    <td style="border: 1px dashed black; padding:2em 1em 2em 1em;margin:1em; text-align:center">
                                        <h3><?php echo $buku->title; ?></h3>
                                        <?php echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($buku->buku_id, $generator::TYPE_CODE_128)) . '">'; ?>
                                        <p style="margin: 0;"><?php echo $buku->buku_id; ?></p>
                                        <p style="margin-bottom: 0;"><b>ISBN</b></p>
                                        <p style="margin: 0;"><?php echo $buku->isbn; ?></p>
                                        <p style="margin-bottom: 0;"><b>Penerbit</b></p>
                                        <p style="margin: 0;"><?php echo $buku->penerbit; ?></p>
                                        <p style="margin-bottom: 0;"><b>Tahun Buku</b></p>
                                        <p style="margin: 0;"><?php echo $buku->thn_buku; ?></p>
                                    </td>
                                    <td style="border: 1px dashed black; padding:2em 1em 2em 1em;margin:1em; text-align:center">
                                        <h3><?php echo $buku->title; ?></h3>
                                        <?php echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($buku->buku_id, $generator::TYPE_CODE_128)) . '">'; ?>
                                        <p style="margin: 0;"><?php echo $buku->buku_id; ?></p>
                                        <p style="margin-bottom: 0;"><b>ISBN</b></p>
                                        <p style="margin: 0;"><?php echo $buku->isbn; ?></p>
                                        <p style="margin-bottom: 0;"><b>Penerbit</b></p>
                                        <p style="margin: 0;"><?php echo $buku->penerbit; ?></p>
                                        <p style="margin-bottom: 0;"><b>Tahun Buku</b></p>
                                        <p style="margin: 0;"><?php echo $buku->thn_buku; ?></p>
                                    </td>
                                </tr>
                                <tr style="border: 1px dashed black;margin:1em;padding:1em;">
                                    <td style="border: 1px dashed black; padding:2em 1em 2em 1em;margin:1em; text-align:center">
                                        <h3><?php echo $buku->title; ?></h3>
                                        <?php echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($buku->buku_id, $generator::TYPE_CODE_128)) . '">'; ?>
                                        <p style="margin: 0;"><?php echo $buku->buku_id; ?></p>
                                        <p style="margin-bottom: 0;"><b>ISBN</b></p>
                                        <p style="margin: 0;"><?php echo $buku->isbn; ?></p>
                                        <p style="margin-bottom: 0;"><b>Penerbit</b></p>
                                        <p style="margin: 0;"><?php echo $buku->penerbit; ?></p>
                                        <p style="margin-bottom: 0;"><b>Tahun Buku</b></p>
                                        <p style="margin: 0;"><?php echo $buku->thn_buku; ?></p>
                                    </td>
                                    <td style="border: 1px dashed black; padding:2em 1em 2em 1em;margin:1em; text-align:center">
                                        <h3><?php echo $buku->title; ?></h3>
                                        <?php echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($buku->buku_id, $generator::TYPE_CODE_128)) . '">'; ?>
                                        <p style="margin: 0;"><?php echo $buku->buku_id; ?></p>
                                        <p style="margin-bottom: 0;"><b>ISBN</b></p>
                                        <p style="margin: 0;"><?php echo $buku->isbn; ?></p>
                                        <p style="margin-bottom: 0;"><b>Penerbit</b></p>
                                        <p style="margin: 0;"><?php echo $buku->penerbit; ?></p>
                                        <p style="margin-bottom: 0;"><b>Tahun Buku</b></p>
                                        <p style="margin: 0;"><?php echo $buku->thn_buku; ?></p>
                                    </td>
                                    <td style="border: 1px dashed black; padding:2em 1em 2em 1em;margin:1em; text-align:center">
                                        <h3><?php echo $buku->title; ?></h3>
                                        <?php echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($buku->buku_id, $generator::TYPE_CODE_128)) . '">'; ?>
                                        <p style="margin: 0;"><?php echo $buku->buku_id; ?></p>
                                        <p style="margin-bottom: 0;"><b>ISBN</b></p>
                                        <p style="margin: 0;"><?php echo $buku->isbn; ?></p>
                                        <p style="margin-bottom: 0;"><b>Penerbit</b></p>
                                        <p style="margin: 0;"><?php echo $buku->penerbit; ?></p>
                                        <p style="margin-bottom: 0;"><b>Tahun Buku</b></p>
                                        <p style="margin: 0;"><?php echo $buku->thn_buku; ?></p>
                                    </td>
                                    <td style="border: 1px dashed black; padding:2em 1em 2em 1em;margin:1em; text-align:center">
                                        <h3><?php echo $buku->title; ?></h3>
                                        <?php echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode($buku->buku_id, $generator::TYPE_CODE_128)) . '">'; ?>
                                        <p style="margin: 0;"><?php echo $buku->buku_id; ?></p>
                                        <p style="margin-bottom: 0;"><b>ISBN</b></p>
                                        <p style="margin: 0;"><?php echo $buku->isbn; ?></p>
                                        <p style="margin-bottom: 0;"><b>Penerbit</b></p>
                                        <p style="margin: 0;"><?php echo $buku->penerbit; ?></p>
                                        <p style="margin-bottom: 0;"><b>Tahun Buku</b></p>
                                        <p style="margin: 0;"><?php echo $buku->thn_buku; ?></p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</section>
</div>