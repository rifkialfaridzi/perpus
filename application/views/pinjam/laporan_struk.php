<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laporan Buku</title>
</head>

<body>
<table cellpadding="1" cellspacing="0" style="border-collapse:collapse;margin:auto; padding:10px; width:90%">

<tbody>
    <tr>
        <td style="text-align:left;width:20%">
            <img style="width: auto;height:120px" src="<?php echo base_url('assets_style/image/logos.jpeg') ?>"></img>
        </td>
        <td style="text-align:center">
            <h2 style="text-align:center;padding:0;margin:0"><strong>SMK Negeri 1 Banyudono</strong></h2>
            <small>Jl. Kuwiran No. 3 Banyudono</small><br>
            <small>No Telp : (0271) 781834 Fax : (0271) 781834</small><br>
            <small>Web : smkn1banyudono.sch.id E-mail : smknbanyudono@gmail.com</small>
        </td>
    </tr>
</tbody>

</table>

    <hr>

    <div style="text-align:center">
    
    <h4 style="text-transform: uppercase;">Struk Peminjaman</h4>
    
        <h4>A. Transaksi</h4>
        <?php //var_dump($buku); ?>
        <p>&nbsp;</p>
        <table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:100%">
            <tbody>
                <tr>

                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Kode Transaksi</b></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Nama Anggota</b></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Status</b></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Lama Pinjam</b></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Tanggal Pinjam</b></td>
                </tr>

                <tr>

                    <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $pinjam->pinjam_id; ?></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $anggota->nama; ?></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $pinjam->status; ?></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $pinjam->lama_pinjam; ?></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $pinjam->tgl_pinjam; ?></td>
                </tr>

            </tbody>
        </table>

        <h4>B. Buku</h4>
        <?php //var_dump($buku); ?>
        <table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:100%">
            <tbody>
                <tr>

                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Kode Buku</b></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Nama Buku</b></td>
                </tr>

                <?php foreach ($buku as $key) { ?>
                <tr>
                    
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key['buku_id']; ?></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $key['title']; ?></td>
                </tr>
                <?php } ?>
            </tbody>
        </table>


        <p>&nbsp;</p>

        <p>&nbsp;</p>

        <table cellpadding="1" cellspacing="0" style="page-break-inside: avoid;text-align:center;border-collapse:collapse; border:none; margin:auto; padding:10px; width:100%">
            <tbody>
                <tr>
                    <td><?php $generator = new Picqer\Barcode\BarcodeGeneratorPNG(); echo '<img src="data:image/png;base64,' . base64_encode($generator->getBarcode("$pinjam->pinjam_id", $generator::TYPE_CODE_128)) . '">';
										?> <br> <?php echo $pinjam->pinjam_id; ?> </td>
                    <!-- <td>Yang Mengetahui,</td> -->
                </tr>
                <tr>
                    <!-- <td><span style="font-size:16px"><strong>Kepala Perpustakaan</strong></span></td> -->
                    <!-- </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Pimpinan<br>Slamet Raharjo</td>
        </tr>
    </tbody>
</table> -->
    </div>
</body>

</html>