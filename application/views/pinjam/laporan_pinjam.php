<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laporan Pinjaman Buku</title>
</head>

<body>
<table cellpadding="1" cellspacing="0" style="border-collapse:collapse;margin:auto; padding:10px; width:90%">

<tbody>
    <tr>
        <td style="text-align:left;width:20%">
            <img style="width: auto;height:120px" src="<?php echo base_url('assets_style/image/logos.jpeg') ?>"></img>
        </td>
        <td style="text-align:center">
            <h2 style="text-align:center;padding:0;margin:0"><strong>SMK Negeri 1 Banyudono</strong></h2>
            <small>Jl. Kuwiran No. 3 Banyudono</small><br>
            <small>No Telp : (0271) 781834 Fax : (0271) 781834</small><br>
            <small>Web : smkn1banyudono.sch.id E-mail : smknbanyudono@gmail.com</small>
        </td>
    </tr>
</tbody>

</table>
    

    <hr>
    <div style="text-align:center">
    <h4 style="text-transform: uppercase;">Laporan Peminjaman</h4>
        <p>&nbsp;</p>
        <table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:100%">
            <tbody>
                <tr>

                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>No Pinjam</b></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Nama Anggota</b></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Tanggal Pinjam</b></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Tanggal Kembali</b></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Status</b></td>
                    <td style="text-align:left; padding: 2px 5px 2px 5px"><b>Denda</b></td>
                </tr>

                <?php

                foreach ($pinjam->result_array() as $isi) {
                    $anggota_id = $isi['anggota_id'];
                    $ang = $this->db->query("SELECT * FROM tbl_login WHERE anggota_id = '$anggota_id'")->row();

                    $pinjam_id = $isi['pinjam_id'];
                    $denda = $this->db->query("SELECT * FROM tbl_denda WHERE pinjam_id = '$pinjam_id'");
                    $total_denda = $denda->row();
                ?>
                    <tr>
                        <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $isi['pinjam_id']; ?></td>
                        <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $ang->nama;; ?></td>
                        <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo  $isi['tgl_pinjam']; ?></td>
                        <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo $isi['tgl_balik']; ?></td>
                        <td style="text-align:left; padding: 2px 5px 2px 5px"><?php echo  $isi['status']; ?></td>
                        <td style="text-align:right; padding: 2px 5px 2px 5px">
                        <?php
												if ($isi['status'] == 'Dibooking') {
													echo '<p style="color:green;">
												Tidak Ada Denda</p>';
												}elseif($isi['status'] == 'Di Kembalikan'){
													echo $this->M_Admin->rp($total_denda->denda);
												}else {
													$jml = $this->db->query("SELECT * FROM tbl_pinjam WHERE pinjam_id = '$pinjam_id'")->num_rows();
													$date1 = date('Ymd');
													$date2 = preg_replace('/[^0-9]/', '', $isi['tgl_balik']);
													$diff = $date1 - $date2;
													if ($diff > 0) {
														echo $diff . ' hari';
														$dd = $this->M_Admin->get_tableid_edit('tbl_biaya_denda', 'stat', 'Aktif');
														echo '<p style="color:red;font-size:18px;">
												' . $this->M_Admin->rp($jml * ($dd->harga_denda * $diff)) . ' 
												</p><small style="color:#333;">* Untuk ' . $jml . ' Buku</small>';
													} else {
														echo '<p style="color:green;">
												Tidak Ada Denda</p>';
													}
												}
												?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

        <p>&nbsp;</p>

        <p>&nbsp;</p>

        <table cellpadding="1" cellspacing="0" style="page-break-inside: avoid;text-align:center;border-collapse:collapse; border:none; margin:auto; padding:10px; width:100%">
            <tbody>
                <tr>
                    <td>Yang Mengetahui,</td>
                </tr>
                <tr>
                    <td><span style="font-size:16px"><strong>Kepala Perpustakaan</strong></span></td> -->
                    <!-- </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Pimpinan<br>Slamet Raharjo</td>
        </tr>
    </tbody>
</table> -->
    </div>
</body>

</html>