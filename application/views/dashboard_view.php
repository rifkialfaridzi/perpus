<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<?php if ($this->session->userdata('level') == 'Anggota') {
  redirect(base_url('transaksi'));
} ?>
<!-- Content Wrapper. Contains page content -->
<!-- Content Header (Page header) -->
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Dashboard <small>Control panel</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Dashboard</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-sm-12">
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?= $count_pengguna; ?></h3>

              <p>Anggota</p>
            </div>
            <div class="icon">
              <i class="fa fa-edit"></i>
            </div>
            <a href="user" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!--small box-->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3><?= $count_buku; ?></h3>

              <p>Jenis Buku</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
            <a href="data" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?= $count_pinjam; ?></h3>

              <p>Pinjam</p>
            </div>
            <div class="icon">
              <i class="fa fa-user-plus"></i>
            </div>
            <a href="transaksi" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h3><?= $count_kembali; ?></h3>

              <p>Di Kembalikan</p>
            </div>
            <div class="icon">
              <i class="fa fa-list"></i>
            </div>
            <a href="transaksi/kembali" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

      </div>
    </div>

    <div class="row">
      <div class="col-sm-12">
        <div class="col-lg-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              Grafik Peminjaman
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <canvas id="myChartPinjaman"></canvas>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="box box-primary">
            <div class="box-header with-border">
              Grafik Pengembalian
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <canvas id="myChartPengembalian"></canvas>
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>
</div>
<!-- /.content -->

<script>

  <?php 
  
  $data_dipinjam = json_encode($dipinjam['dipinjam']);
  $data_dipinjam_tanggal = json_encode($dipinjam['dipinjam_tanggal']);

  $data_dikembalikan = json_encode($dikembalikan['dikembalikan']);
  $data_dikembalikan_tanggal = json_encode($dikembalikan['dikembalikan_tanggal']);
  
  
  ?>

  var ctx = document.getElementById("myChartPinjaman").getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: <?php echo $data_dipinjam_tanggal; ?>,
      datasets: [{
        label: '# Grafik Peminjaman',
        data: <?php echo $data_dipinjam; ?>,
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)'
        ],
        borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });

  var ctxs = document.getElementById("myChartPengembalian").getContext('2d');
  var myCharts = new Chart(ctxs, {
    type: 'bar',
    data: {
      labels: <?php echo $data_dikembalikan_tanggal; ?>,
      datasets: [{
        label: '# Grafik Pengembalian',
        data: <?php echo $data_dikembalikan; ?>,
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(255, 206, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(255, 159, 64, 0.2)'
        ],
        borderColor: [
          'rgba(255,99,132,1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ],
        borderWidth: 1
      }]
    },
    options: {
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });

</script>