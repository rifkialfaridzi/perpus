<?php if(! defined('BASEPATH')) exit('No direct script acess allowed');?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      <i class="fa fa-edit" style="color:green"> </i>  <?= $title_web;?>
    </h1>
    <ol class="breadcrumb">
			<li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i>&nbsp; Dashboard</a></li>
			<li class="active"><i class="fa fa-file-text"></i>&nbsp; <?= $title_web;?></li>
    </ol>
  </section>
  <section class="content">
	<?php if(!empty($this->session->flashdata())){ echo $this->session->flashdata('pesan');}?>
	<div class="row">
	    <div class="col-md-12">
	        <div class="box box-primary">
                <div class="box-header with-border">
					<?php if($this->session->userdata('level') == 'Petugas'){?>
                    <a target="_blank" href="<?php echo base_url("bukutamu/create"); ?>"><button class="btn btn-primary">
						<i class="fa fa-plus"> </i> Tambah Buku Tamu</button></a>
					<?php }?>

                    <div class="pull-right">
						<a href="<?php echo base_url("bukutamu/cetak/"); ?>" target="_blank" class="btn btn-success btn-md" style="margin-left:1pc;">
							<i class="fa fa-print"></i>&nbsp; Cetak Laporan</a>
						</div>
                </div>
				<!-- /.box-header -->
				<div class="box-body">
                    <br/>
					<div class="table-responsive">
                    <table id="example1" class="table table-bordered table-striped table" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Jenis</th>
                                <th>Tanggal</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no=1;foreach($buku as $isi){?>
                            <tr>
                                <td><?php echo $no;?></td>
                                <td><?php echo $isi->nama; ?></td>
                                <td><?php echo $isi->email; ?></td>
                                <td><?php echo $isi->jenis; ?></td>
                                <td><?php echo $isi->tanggal;?></td>
								<td>
                                <a href="<?= base_url('bukutamu/delete/'.$isi->id);?>" onclick="return confirm('Anda yakin Buku ini akan dihapus ?');">
									<button class="btn btn-danger"><i class="fa fa-trash"></i></button></a>
								</td>
                            </tr>
                        <?php $no++;}?>
                        </tbody>
                    </table>
			    </div>
			    </div>
	        </div>
    	</div>
    </div>
</section>
</div>