<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Laporan User</title>
</head>

<body>
    <table cellpadding="1" cellspacing="0" style="border-collapse:collapse;margin:auto; padding:10px; width:90%">

        <tbody>
            <tr>
                <td style="text-align:left;width:20%">
                    <img style="width: auto;height:120px" src="<?php echo base_url('assets_style/image/logos.jpeg') ?>"></img>
                </td>
                <td style="text-align:center">
                    <h2 style="text-align:center;padding:0;margin:0"><strong>SMK Negeri 1 Banyudono</strong></h2>
                    <small>Jl. Kuwiran No. 3 Banyudono</small><br>
                    <small>No Telp : (0271) 781834 Fax : (0271) 781834</small><br>
                    <small>Web : smkn1banyudono.sch.id E-mail : smknbanyudono@gmail.com</small>
                </td>
            </tr>
        </tbody>

    </table>


    <hr>
    <div style="text-align:center">
        <h4 style="text-transform: uppercase;">Laporan User</h4>
        <p>&nbsp;</p>
        <table border="1" cellpadding="1" cellspacing="0" style="border-collapse:collapse; border:1px solid black; margin:auto; padding:10px; width:100%">
            <tbody>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Username</th>
                    <th>Jenis Kelamin</th>
                    <th>Telepon</th>
                    <th>Level</th>
                    <th>Alamat</th>
                </tr>

                <?php
                $counter = 1;
                foreach ($user as $isi) {

                ?>
                    <tr>
                        <td><?= $counter++; ?></td>
                        <td><?= $isi['nama']; ?></td>
                        <td><?= $isi['user']; ?></td>
                        <td><?= $isi['jenkel']; ?></td>
                        <td><?= $isi['telepon']; ?></td>
                        <td><?= $isi['level']; ?></td>
                        <td><?= $isi['alamat']; ?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

        <p>&nbsp;</p>

        <p>&nbsp;</p>

        <table cellpadding="1" cellspacing="0" style="page-break-inside: avoid;text-align:center;border-collapse:collapse; border:none; margin:auto; padding:10px; width:100%">
            <tbody>
                <tr>
                    <td>Yang Mengetahui,</td>
                </tr>
                <tr>
                    <td><span style="font-size:16px"><strong>Kepala Perpustakaan</strong></span></td> -->
                    <!-- </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Pimpinan<br>Slamet Raharjo</td>
        </tr>
    </tbody>
</table> -->
    </div>
</body>

</html>