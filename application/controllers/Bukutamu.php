<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bukutamu extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        //validasi jika user belum login
        $this->data['CI'] = &get_instance();
        $this->load->helper(array('form', 'url'));
        $this->load->model('M_Admin');
        $this->load->model('Bukutamu_model');
        if ($this->session->userdata('masuk_perpus') != TRUE) {
            $url = base_url('login');
            redirect($url);
        }
    }

    public function index()
    {
        $this->data['idbo'] = $this->session->userdata('ses_id');
        $this->data['buku'] =  $this->Bukutamu_model->get_all();
        $this->data['title_web'] = 'Data Buku Tamu';

        $this->load->view('header_view', $this->data);
        $this->load->view('sidebar_view', $this->data);
        $this->load->view('bukutamu/bukutamu_view', $this->data);
        $this->load->view('footer_view', $this->data);
    }
    public function create()
    {
        $this->data['idbo'] = $this->session->userdata('ses_id');
        $this->data['buku'] =  $this->Bukutamu_model->get_all();
        $this->data['title_web'] = 'Data Buku Tamu';

        $this->load->view('bukutamu/create_bukutamu', $this->data);
    }

    public function create_action()
    {   
        $data=$this->input->post();
        $data['tanggal'] = date("Y-m-d H:i:s");
        $this->Bukutamu_model->insert($data);
        $this->session->set_flashdata('pesan', 'Data Sukses Disimpan');
        redirect(site_url('bukutamu/create'));
    }

    public function delete($id)
    {
        $this->Bukutamu_model->delete($id);
        $this->session->set_flashdata('pesan', '<div id="notifikasi"><div class="alert alert-warning">
        <p> Berhasil Hapus Buku !</p>
    </div></div>');
        redirect(base_url('bukutamu'));
    }
    public function cetak(){
            $data = $this->Bukutamu_model->get_all();


			$this->load->library('pdf');

			$this->pdf->setPaper('A4', 'potrait');
			$this->pdf->set_option('isRemoteEnabled', TRUE);
			$this->pdf->filename = "Laporan Buku Tamu.pdf";
			$this->pdf->load_view('bukutamu/laporan_bukutamu', ['bukutamu' => $data]);
    }
}
