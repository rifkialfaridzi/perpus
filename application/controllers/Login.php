<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        //validasi jika user belum login
        $this->data['CI'] = &get_instance();
        $this->load->helper(array('form', 'url'));
        $this->load->model('M_login');
        $this->load->model('M_Admin');
    }
    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        $this->data['title_web'] = 'Login | Sistem Informasi Perpustakaan';
        $this->load->view('login_view', $this->data);
    }

    public function registrasi()
    {
        $this->data['title_web'] = 'Login | Sistem Informasi Perpustakaan';
        $this->load->view('registrasi_view', $this->data);
    }

    public function auth()
    {
        $user = htmlspecialchars($this->input->post('user', TRUE), ENT_QUOTES);
        $pass = htmlspecialchars($this->input->post('pass', TRUE), ENT_QUOTES);
        // auth
        $proses_login = $this->db->query("SELECT * FROM tbl_login WHERE user='$user' AND pass = md5('$pass')");
        $row = $proses_login->num_rows();
        if ($row > 0) {
            $hasil_login = $proses_login->row_array();

            // create session
            $this->session->set_userdata('masuk_perpus', TRUE);
            $this->session->set_userdata('level', $hasil_login['level']);
            $this->session->set_userdata('ses_id', $hasil_login['id_login']);
            $this->session->set_userdata('anggota_id', $hasil_login['anggota_id']);

            echo '<script>window.location="' . base_url() . 'dashboard";</script>';
        } else {

            echo '<script>alert("Login Gagal, Periksa Kembali Username dan Password Anda");
            window.location="' . base_url() . '"</script>';
        }
    }

    public function register()
    {
        $user = htmlspecialchars($this->input->post('user', TRUE), ENT_QUOTES);
        $pass = htmlspecialchars($this->input->post('pass', TRUE), ENT_QUOTES);
        $nama = htmlspecialchars($this->input->post('nama', TRUE), ENT_QUOTES);
        $nisn = htmlspecialchars($this->input->post('nisn', TRUE), ENT_QUOTES);
        $kelas = htmlspecialchars($this->input->post('kelas', TRUE), ENT_QUOTES);
        $nama = htmlspecialchars($this->input->post('nama', TRUE), ENT_QUOTES);
        $jenkel = htmlspecialchars($this->input->post('jenkel', TRUE), ENT_QUOTES);
        $telepon = htmlspecialchars($this->input->post('telepon', TRUE), ENT_QUOTES);
        $email = htmlspecialchars($this->input->post('email', TRUE), ENT_QUOTES);

        $this->form_validation->set_rules(
            'user',
            'Username',
            'required|min_length[5]|max_length[12]|is_unique[tbl_login.user]',
            array(
                'required'      => 'You have not provided %s.',
                'is_unique'     => 'This %s already exists.'
            )
        );
        $this->form_validation->set_rules('pass', 'Password', 'required');
        $this->form_validation->set_rules('nama', 'Nama Lengkap', 'required');
        $this->form_validation->set_rules('jenkel', 'Jenis Kelamin', 'required');
        $this->form_validation->set_rules(
            'telepon',
            'Telepon',
            'required|min_length[5]|is_unique[tbl_login.telepon]',
            array(
                'required'      => 'You have not provided %s.',
                'is_unique'     => 'This %s already exists.'
            )
        );
        $this->form_validation->set_rules(
            'nisn',
            'NISN',
            'required|min_length[5]|is_unique[tbl_login.nisn]',
            array(
                'required'      => 'You have not provided %s.',
                'is_unique'     => 'This %s already exists.'
            )
        );
        $this->form_validation->set_rules(
            'email',
            'Email',
            'required|min_length[5]|is_unique[tbl_login.email]',
            array(
                'required'      => 'You have not provided %s.',
                'is_unique'     => 'This %s already exists.'
            )
        );

        if ($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('pesan','<div id="notifikasi"><div class="alert alert-danger">
            <p> '.validation_errors() .'</p>
            </div></div>');
			redirect(base_url('/login/registrasi'));
        } else {
            $id = $this->M_Admin->buat_kode('tbl_login','AG','id_login','ORDER BY id_login DESC LIMIT 1'); 
    
            $data = array(
				'anggota_id' => $id,
                'nama'=>$nama,
                'user'=>$user,
                'pass'=>md5($pass),
                'level'=>"Anggota",
                'tempat_lahir'=>"-",
                'tgl_lahir'=>"-",
                'email'=>$email,
                'telepon'=>$telepon,
                'nisn'=>$nisn,
                'kelas'=>$kelas,
                'foto'=>"dummy.jpg",
                'jenkel'=>$jenkel,
                'alamat'=>"-",
                'tgl_bergabung'=>date('Y-m-d')
            );
            $this->M_login->insert($data);
            $this->session->set_flashdata('pesan','<div id="notifikasi"><div class="alert alert-success">
            <p> Pendaftaran User telah berhasil !</p>
            </div></div>');
			redirect(base_url('/login/registrasi'));
        }

        

        // // auth
        // $proses_login = $this->db->query("SELECT * FROM tbl_login WHERE user='$user' AND pass = md5('$pass')");
        // $row = $proses_login->num_rows();
        // if($row > 0)
        // {
        //     $hasil_login = $proses_login->row_array();

        //     // create session
        //     $this->session->set_userdata('masuk_perpus',TRUE);
        //     $this->session->set_userdata('level',$hasil_login['level']);
        //     $this->session->set_userdata('ses_id',$hasil_login['id_login']);
        //     $this->session->set_userdata('anggota_id',$hasil_login['anggota_id']);

        //     echo '<script>window.location="'.base_url().'dashboard";</script>';
        // }else{

        //     echo '<script>alert("Login Gagal, Periksa Kembali Username dan Password Anda");
        //     window.location="'.base_url().'"</script>';
        // }
    }

    public function logout()
    {
        $this->session->sess_destroy();
        echo '<script>window.location="' . base_url() . '";</script>';
    }
}
