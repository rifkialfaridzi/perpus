<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	function __construct(){
	 parent::__construct();
	 	//validasi jika user belum login
     $this->data['CI'] =& get_instance();
     $this->load->helper(array('form', 'url'));
     $this->load->model('M_Admin');
     $this->load->model('Buku_model');
	 	 if($this->session->userdata('masuk_perpus') != TRUE){
				 $url=base_url('login');
				 redirect($url);
		 }
	 }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
		$data_dipinjam = $this->Buku_model->data_statistik("Dipinjam");
		$data_dikembalikan = $this->Buku_model->data_statistik("Di Kembalikan");
		$dipinjam = [];
		$dipinjam_tanggal = [];
		foreach ($data_dipinjam as $key) {
			$dipinjam[] = $key->jumlah_status;
			$dipinjam_tanggal[] = date('F', strtotime($key->tgl_pinjam));
		}

		$dikembalikan = [];
		$dikembalikan_tanggal = [];
		foreach ($data_dikembalikan as $key) {
			$dikembalikan[] = $key->jumlah_status;
			$dikembalikan_tanggal[] = date('F', strtotime($key->tgl_pinjam));
		}

		$dashboard_dipinjam['dipinjam']= $dipinjam;
		$dashboard_dipinjam['dipinjam_tanggal']= $dipinjam_tanggal;

		$dashboard_dikembalikan['dikembalikan']= $dikembalikan;
		$dashboard_dikembalikan['dikembalikan_tanggal']= $dikembalikan_tanggal;


		$this->data['idbo'] = $this->session->userdata('ses_id');
		$this->data['title_web'] = 'Dashboard ';
		$this->data['dipinjam'] = $dashboard_dipinjam;
		$this->data['dikembalikan'] = $dashboard_dikembalikan;
		$this->data['count_pengguna']=$this->db->query("SELECT * FROM tbl_login")->num_rows();
		$this->data['count_buku']=$this->db->query("SELECT * FROM tbl_buku")->num_rows();
		$this->data['count_pinjam']=$this->db->query("SELECT * FROM tbl_pinjam WHERE status = 'Dipinjam'")->num_rows();
		$this->data['count_kembali']=$this->db->query("SELECT * FROM tbl_pinjam WHERE status = 'Di Kembalikan'")->num_rows();
		$this->load->view('header_view',$this->data);
		$this->load->view('sidebar_view',$this->data);
		$this->load->view('dashboard_view',$this->data);
		$this->load->view('footer_view',$this->data);
	}

	function data(){
		
		$data_dipinjam = $this->Buku_model->data_statistik("Dipinjam");
		$data_dikembalikan = $this->Buku_model->data_statistik("Di Kembalikan");
		
		foreach ($data_dipinjam as $key) {
			$dipinjam[] = $key->jumlah_status;
			$dipinjam_tanggal[] = date('F', strtotime($key->tgl_pinjam));
		}

		foreach ($data_dikembalikan as $key) {
			$dikembalikan[] = $key->jumlah_status;
			$dikembalikan_tanggal[] = date('F', strtotime($key->tgl_pinjam));
		}

		$dashboard_dipinjam['dipinjam']= $dipinjam;
		$dashboard_dipinjam['dipinjam_tanggal']= $dipinjam_tanggal;

		$dashboard_dikembalikan['dikembalikan']= $dikembalikan;
		$dashboard_dikembalikan['dikembalikan_tanggal']= $dikembalikan_tanggal;

		var_dump($data_dipinjam);
	}

}
